.. _runtime-environment:

===================
Runtime environment
===================

debusine is primarily developed to run on Debian 12 'Bookworm'
with all the dependencies coming from the standard Debian
repositories.

To run debusine please use the provided packages.
