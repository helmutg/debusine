.. _development-blueprints:

======================
Development blueprints
======================

.. toctree::

    debootstrap-task
    acl
    workflows
