# Copyright 2024 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Ontology definition of SystemImageBuild."""
import abc

from debusine.tasks import RunCommandTask
from debusine.tasks.models import BaseDynamicTaskData, SystemImageBuildData


class SystemImageBuild(
    abc.ABC, RunCommandTask[SystemImageBuildData, BaseDynamicTaskData]
):
    """Implement ontology SystemImageBuild."""

    TASK_VERSION = 1
