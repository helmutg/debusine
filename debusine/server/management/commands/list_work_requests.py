# Copyright 2022 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""debusine-admin command to list work requests."""

from debusine.db.models import WorkRequest
from debusine.server.management.debusine_base_command import DebusineBaseCommand
from debusine.server.management.utils import print_work_requests


class Command(DebusineBaseCommand):
    """Command to list work requests."""

    help = "List work requests. Sorted by 'created_at'"

    def handle(self, *args, **options):
        """List the work requests."""
        work_requests = WorkRequest.objects.all().order_by('created_at')
        print_work_requests(self.stdout, work_requests)
