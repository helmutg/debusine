# Generated by Django 3.2.19 on 2024-04-10 14:52

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('db', '0037_workrequest_internal_collection'),
    ]

    operations = [
        migrations.AlterField(
            model_name='workrequest',
            name='parent',
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name='children',
                to='db.workrequest',
            ),
        ),
    ]
