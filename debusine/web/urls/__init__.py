# Copyright 2023 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.
"""Django pages URL's for Debusine."""
from django.urls import include, path

urlpatterns = [
    path("", include("debusine.web.urls.homepage", namespace="homepage")),
    path(
        "accounts/",
        include("debusine.web.urls.accounts", namespace="accounts"),
    ),
    path(
        "work-request/",
        include("debusine.web.urls.work_requests", namespace="work-requests"),
    ),
    path(
        "workspace/",
        include("debusine.web.urls.workspaces", namespace="workspaces"),
    ),
    path(
        "artifact/",
        include(
            "debusine.web.urls.artifacts",
            namespace="artifacts",
        ),
    ),
    path(
        "user/",
        include(
            "debusine.web.urls.user",
            namespace="user",
        ),
    ),
]
