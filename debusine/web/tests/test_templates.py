# Copyright 2023 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""
Tests for generic templates or included templates.

The majority of template code is tested via test_views.py. Any other specific
code for templates is tested in this file.
"""
from django.core.handlers import exception
from django.template.loader import get_template
from django.test import TestCase
from django.urls import Resolver404

from debusine.db.models import WorkRequest
from debusine.web.views.tests.test_views import (
    _html_work_request_result,
    _html_work_request_status,
)


class TemplateTests(TestCase):
    """Tests for the project level templates."""

    def test_404_render_exception(self):
        """404.html template renders the exception message."""
        template = get_template("404.html")
        exception_msg = "This is an exception message"
        context = {"exception": exception_msg}

        rendered_template = template.render(context)

        self.assertIn(exception_msg, rendered_template)

    def test_404_render_error(self):
        """404.html render error."""
        template = get_template("404.html")
        error = "This is an error message"
        context = {"error": error}

        self.assertIn(error, template.render(context))

    def test_404_no_error_no_useful_exception(self):
        """
        Template 404.html does not render exception.__class__.__name.__.

        In 404.html it's hardcoded as "Resolver404".
        """
        template = get_template("404.html")
        context = {"exception": Resolver404()}
        rendered_template = template.render(context)

        self.assertNotIn(str(exception.__class__.__name__), rendered_template)


class WorkRequestResultTests(TestCase):
    """Tests for _work_request-result.html."""

    def setUp(self):
        """Initialize test."""
        self.template = get_template("web/_work_request-result.html")

    def test_work_request_results(self):
        """Test rendering for success, failure and error results."""
        for result in ["success", "failure", "error"]:
            with self.subTest(result=result):
                rendered_template = self.template.render({"result": result})
                self.assertHTMLEqual(
                    rendered_template, _html_work_request_result(result)
                )

    def test_work_request_other(self):
        """Test for result is empty."""
        context = {"result": ""}
        rendered_template = self.template.render(context)
        self.assertHTMLEqual(rendered_template, "")


class WorkRequestStatusTests(TestCase):
    """Tests for _work_request-status.html."""

    def setUp(self):
        """Initialize test."""
        self.template = get_template("web/_work_request-status.html")

    def test_work_request_status(self):
        """Test rendering for different status."""
        for status in WorkRequest.Statuses.choices:
            status = status[0]
            with self.subTest(status=status):
                rendered_template = self.template.render({"status": status})

                self.assertHTMLEqual(
                    rendered_template, _html_work_request_status(status)
                )

    def test_work_request_unexpected(self):
        """Test rendering function raise assertion."""
        with self.assertRaises(AssertionError):
            _html_work_request_status("")
