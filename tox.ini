# Tox (http://tox.testrun.org/) is a tool for running tests
# in multiple virtualenvs. This configuration file will run the
# test suite on all supported python versions. To use it, "pip install tox"
# and then run "tox" from this directory.

[tox]
envlist =
    {py311,py312}-{django32}-server-unit-tests,
    {py311,py312}-client-tests,
    pyupgrade,
    flake8,
    black,
    format,
    codespell,
    mypy,
# Ensure we have no warnings on last Django LTS
    py311-django32-server-no-warnings,
# Ensure we don't have missing migration files
    migrations
skipsdist = True
skip_missing_interpreters = True

[main]
django_debusine_packages = debusine.db debusine.server debusine.web debusine.project debusine.test

[testenv]
setenv =
    LANG=C
    PYTHONASYNCIODEBUG=1
    PYTHONDEBUG=1
passenv =
    PGHOST
    PGPASSWORD
    PGPORT
    PGUSER
    PYTHONDEVMODE
commands =
    server-unit-tests: {envpython} ./manage.py test {[main]django_debusine_packages} {posargs}
    client-tests:
        {envpython} -m unittest discover debusine.artifacts {posargs}
        {envpython} -m unittest discover debusine.client {posargs}
        {envpython} -m unittest discover debusine.tasks {posargs}
        {envpython} -m unittest discover debusine.utils {posargs}
        {envpython} -m unittest discover debusine.worker {posargs}
    server-no-warnings: {envpython} -W error ./manage.py test {[main]django_debusine_packages} {posargs}
    server-show-warnings: {envpython} -W all ./manage.py test {[main]django_debusine_packages} {posargs}
    check: {envpython} ./manage.py check {posargs}
    migrations: {envpython} ./manage.py makemigrations --check --dry-run {posargs}
deps =
    -e.
    django32: Django>=3.2,<3.3
    server: -e.[server,tests]

[testenv:pyupgrade]
commands = pyup-dirs --recursive --keep-percent-format --py311-plus debusine {posargs}
deps = pyupgrade-directories

[testenv:flake8]
commands = {envpython} -m flake8 --docstring-convention=all --unused-arguments-ignore-variadic-names --unused-arguments-ignore-stub-functions debusine/ debian/tests/ {posargs}
deps =
    flake8
    flake8-absolute-import
    flake8-builtins
    flake8-docstrings
    flake8-import-order
    flake8-logging-format
    flake8-rst-docstrings
    flake8-unused-arguments

[testenv:black]
# Disable the PYTHON*DEBUG variables that generate noise in the black output
setenv = LANG=C.UTF-8
commands = {envpython} -m black --check --diff debusine/ debian/tests/ {posargs}
deps = black>=22.1

[testenv:format]
# Disable the PYTHON*DEBUG variables that generate noise in the black output
setenv = LANG=C.UTF-8
commands = {envpython} -m black --diff debusine/ debian/tests/ {posargs}
           {envpython} -m black debusine/ debian/tests/ {posargs}
           {envpython} -m djlint --reformat debusine/web/templates {posargs}
deps = black>=22.1
       djlint

[testenv:djlint]
setenv = LANG=C.UTF-8
commands = {envpython} -m djlint --check debusine/web/templates {posargs}
           {envpython} -m djlint --lint debusine/web/templates {posargs}
deps = djlint

[testenv:codespell]
allowlist_externals = codespell
commands = codespell --skip="./data/*" {posargs}
deps = codespell

[testenv:mypy]
commands = mypy {posargs}
deps =
    mypy
    .[server,tests]
    django-stubs[compatible-mypy]<5
    djangorestframework-stubs[compatible-mypy]
    pydantic[email]
    types-psutil
    types-python-dateutil
    types-PyYAML
    types-requests
    types-tabulate
    types-lxml

[testenv:shellcheck]
allowlist_externals = sh
commands = sh -c '\
    (git ls-files | file --mime-type --no-pad --no-buffer --files-from - | \
     grep ": text/x-shellscript\$" | cut -d: -f1; \
     echo debian/tests/utils/add-debusine-log-files-to-artifacts.sh) \
    | xargs shellcheck'
deps =
    shellcheck-py

[flake8]
max-complexity = 12
max-line-length = 80
exclude = .git,.ropeproject,.tox,__pycache__,debusine/project/settings/local.py,docs/conf.py,*/migrations/*.py
ignore =
# class attribute shadow a builtin function
    A003,
# function name should be lowercase
    N802,
# line break after/before binary operator
    W504, W503
# sphinx's roles unknown to flake8-rst-docstrings until
# https://github.com/peterjc/flake8-rst-docstrings/issues/7
    RST304,
# rules for flake8-docstrings
# docstring - "Missing docstring in public nested class"
    D106,
# docstring - "Multi-line docstring summary should start at the first line"
    D212,
# docstring - "1 blank line required before class docstring"
    D203,
# https://black.readthedocs.io/en/stable/the_black_code_style/current_style.html#flake8
# whitespace before ':' (conflicts with black)
    E203,
# multiple statements on one line (def) (conflicts with black)
    E704
builtins-allowed-modules =
# Shadowed by debusine.server.collections, but since we always use absolute
# imports this doesn't cause a problem.
    collections,
enable-extensions = G
application-import-names = debusine
rst-directives = graphviz
